class NoteController {
	itemList:Array<Note> = [];
	editItem:Note;

	// provide angular injection
	constructor(private $log:ILogService) {
		...
	}

	// method available in the view as "$ctrl.editItem()"
	editItem():void {
		this.$log.info('submitNewItem');
		...
	}

	saveItem():void {
		this.$log.info('submitEditItem');
		...
	}
}

angular.module('app')
	.controller('NoteController', NoteController)
;
