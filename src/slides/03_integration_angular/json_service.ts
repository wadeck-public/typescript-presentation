class JsonService {
	constructor(private $log:ILogService, private $q:IQService) {
	}

	minify(content:string):IPromise<string> {
		try {
			const contentJson = JSON.parse(content);
			return this.$q.resolve(JSON.stringify(contentJson, null, 0));
		}
		catch (e) {
			return this.$q.reject('Invalid JSON');
		}
	}
}

angular.module('app')
	.service('JsonService', JsonService)
;
