const names1:Array<string> = ['a', 'b'];
const names2:string[] = ['a', 'b'];

interface Pair<A, B> {
	getA():A
	getB():B
}