async () => {
	const user = await retrieveUser()
	const profile = await retrieveProfile(user.id)
	const tasks = await retrieveTasks(user, profile)
	...
}


async () => {
	const user = await retrieveUser()
	const profile = await retrieveProfile(user.id)
	let tasks;
	if(profile.isAdmin){
		tasks = await retrieveAllTasks()
	}else{
		tasks = await retrieveTasks(user, profile)
	}
	...
}
