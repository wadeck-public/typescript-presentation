type Callable = {name:string}
type Human = {age:number}

type PersonOne = Human | Callable
interface PersonBoth extends Human, Callable {}

const toto:PersonOne = {name:'toto'};
const tata:PersonOne = {age:12};
const titi:Human | Callable = {age:12};
const tutu:PersonBoth = {name:'tutu', age:12};