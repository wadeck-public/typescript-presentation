const doStuff = (a:number, b?:number, c = 4) => {
	if(b === undefined){
		return a + c;
	}else{
		return a + b + c;
	}
}

doStuff(1) // 5
doStuff(1, 2) // 7
doStuff(1, 2, 3) // 6