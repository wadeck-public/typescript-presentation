function increment(a){
	return a + 1;
}

const increment: (a: number) => number = a => a + 1;