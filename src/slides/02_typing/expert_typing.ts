interface Factory<T> {
	():T
}

const factory:Factory<string> = ...
const aString = factory();


interface Newable<T> {
	new ():T
}

const newable:Newable<string> = ...
const aString = new newable();


let weird:(a:number) => (b:string) => number;
weird = (a) => (b) => a + b.length;
weird(13)('toto');

var weird;
weird = function (a) {
	return function (b) {
		return a + b.length;
	};
};
weird(13)('toto');
