const person:{name:string} = {name:'toto'};

interface Person {
	name:string
}
const person2:Person = {name:'toto'};

type PersonType = {name:string};
const person3:PersonType = {name:'toto'};