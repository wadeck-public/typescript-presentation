const a:boolean = true;

// implicitly typed to boolean
// during compilation
const a2 = true;

const b:string = 'toto';

const c:number = 4.75;

const d:Array = [];

const e:Object = {};

const f:{a:number} = {a:3};