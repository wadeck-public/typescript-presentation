interface Person {
	firstName?:string
	lastName?:string
	age:number
}

const p:Person = {age:12};
p.firstName = 'Peter';
