console.log('Welcome the fellowship!')

['Frodo', 'Gandalf', 'Legolas'].forEach(function(name){
	hello(name)
})

function hello(name){
	console.log('Hello, ' + name + '!')
}

=> Hello, Frodo!
   Hello, Gandalf!
   ...

=> hello is undefined

=> Uncaught TypeError: Cannot read property 'Legolas' of undefined